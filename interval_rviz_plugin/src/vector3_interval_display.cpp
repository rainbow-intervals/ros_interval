/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/properties/int_property.h>
#include <rviz/frame_manager.h>

#include "vector3_interval_visual.h"
#include "vector3_interval_display.h"

namespace interval_rviz_plugin
{



Vector3IntervalDisplay::Vector3IntervalDisplay()
{
  box_color_property_ = new rviz::ColorProperty( "Box Color", QColor( 125, 255, 0 ),
                                             "Color to draw the box.",
                                             this, SLOT( updateColorAndAlpha() ));

  box_alpha_property_ = new rviz::FloatProperty( "Box Alpha", 0.3,
                                             "0 is fully transparent, 1.0 is fully opaque.",
                                             this, SLOT( updateColorAndAlpha() ));


  boundaries_vectors_color_property_ = new rviz::ColorProperty( "Boundaries Vectors Color", QColor( 0, 255, 255 ),
                                              "Color to draw the boundaries arrows.",
                                             this, SLOT( updateColorAndAlpha() ));

  boundaries_vectors_alpha_property_ = new rviz::FloatProperty( "Boundaries Vectors Alpha", 0.2,
                                              "0 is fully transparent, 1.0 is fully opaque.",
                                             this, SLOT( updateColorAndAlpha() ));

  boundaries_vectors_visibility_property_ = new rviz::BoolProperty( "Hide Boundaries Vectors", true,
                                              "Option for the display of the boundaries vectors",
                                              this, SLOT( updateColorAndAlpha() ));

  central_vector_color_property_ = new rviz::ColorProperty( "Central Vector Color", QColor( 255, 0, 255 ),
                                              "Color to draw the central arrow.",
                                             this, SLOT( updateColorAndAlpha() ));

  central_vector_alpha_property_ = new rviz::FloatProperty( "Central Vector Alpha", 0.9,
                                              "0 is fully transparent, 1.0 is fully opaque.",
                                             this, SLOT( updateColorAndAlpha() ));

  central_vector_visibility_property_ = new rviz::BoolProperty( "Hide Central Vector", false,
                                              "Option for the display of the central vector",
                                             this, SLOT( updateColorAndAlpha() ));

//Set of min and max for alpha parameters
  box_alpha_property_->setMin( 0 );
  box_alpha_property_->setMax( 1 );
  boundaries_vectors_alpha_property_->setMin( 0 );
  boundaries_vectors_alpha_property_->setMax( 1 );
  central_vector_alpha_property_->setMin( 0 );
  central_vector_alpha_property_->setMax( 1 );

  flag_received_msg = false;
}

// After the top-level rviz::Display::initialize() does its own setup,
// it calls the subclass's onInitialize() function.  This is where we
// instantiate all the workings of the class.  We make sure to also
// call our immediate super-class's onInitialize() function, since it
// does important stuff setting up the message filter.

void Vector3IntervalDisplay::onInitialize()
{
  MFDClass::onInitialize();
}

Vector3IntervalDisplay::~Vector3IntervalDisplay()
{
}

// Clear the visual by deleting its objects.
void Vector3IntervalDisplay::reset()
{
  MFDClass::reset();

}

// Set the current color and alpha values for each visual.
void Vector3IntervalDisplay::updateColorAndAlpha()
{
  float box_alpha = box_alpha_property_->getFloat();
  float boundaries_vectors_alpha = boundaries_vectors_alpha_property_->getFloat();
  float central_vector_alpha = central_vector_alpha_property_->getFloat();
  Ogre::ColourValue box_color = box_color_property_->getOgreColor();
  Ogre::ColourValue boundaries_vectors_color = boundaries_vectors_color_property_->getOgreColor();
  Ogre::ColourValue central_vector_color = central_vector_color_property_->getOgreColor();
  bool boundaries_vectors_hidden = boundaries_vectors_visibility_property_->getBool();
  bool central_vector_hidden = central_vector_visibility_property_->getBool();

  if (flag_received_msg)
  {
    visual->setBoxColor( box_color.r, box_color.g, box_color.b, box_alpha);
    visual->setBoundariesVectorsColor( boundaries_vectors_color.r, boundaries_vectors_color.g,
                                      boundaries_vectors_color.b, boundaries_vectors_alpha, boundaries_vectors_hidden);
    visual->setCentralVectorColor( central_vector_color.r, central_vector_color.g, central_vector_color.b,
                                    central_vector_alpha, central_vector_hidden);
  }
}


// This is our callback to handle an incoming message.
void Vector3IntervalDisplay::processMessage( const interval_msgs::Vector3IntervalStamped::ConstPtr& msg )
{
  // Here we call the rviz::FrameManager to get the transform from the
  // fixed frame to the frame in the header of this Vector3IntervalStamped message.  If
  // it fails, we can't do anything else so we return.
  Ogre::Quaternion orientation;
  Ogre::Vector3 position;

  flag_received_msg = true;

  if( !context_->getFrameManager()->getTransform( msg->header.frame_id,
                                                  msg->header.stamp,
                                                  position, orientation ))
  {
    ROS_DEBUG( "Error transforming from frame '%s' to frame '%s'",
               msg->header.frame_id.c_str(), qPrintable( fixed_frame_ ));
    return;
  }


  visual.reset(new Vector3IntervalVisual( context_->getSceneManager(), scene_node_ ));
  // Now set or update the contents of the chosen visual.
  visual->setMessage( msg );
  visual->setFramePosition( position );
  visual->setFrameOrientation( orientation );

  float box_alpha = box_alpha_property_->getFloat();
  float boundaries_vectors_alpha = boundaries_vectors_alpha_property_->getFloat();
  float central_vector_alpha = central_vector_alpha_property_->getFloat();
  Ogre::ColourValue box_color = box_color_property_->getOgreColor();
  Ogre::ColourValue boundaries_vectors_color = boundaries_vectors_color_property_->getOgreColor();
  Ogre::ColourValue central_vector_color = central_vector_color_property_->getOgreColor();
  bool boundaries_vectors_hidden = boundaries_vectors_visibility_property_->getBool();
  bool central_vector_hidden = central_vector_visibility_property_->getBool();

  visual->setBoxColor( box_color.r, box_color.g, box_color.b, box_alpha);
  visual->setBoundariesVectorsColor( boundaries_vectors_color.r, boundaries_vectors_color.g,
                                    boundaries_vectors_color.b, boundaries_vectors_alpha, boundaries_vectors_hidden);
  visual->setCentralVectorColor( central_vector_color.r, central_vector_color.g, central_vector_color.b,
                                  central_vector_alpha, central_vector_hidden);


}

} // end namespace rviz_plugin_tutorials

// Tell pluginlib about this class.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(interval_rviz_plugin::Vector3IntervalDisplay,rviz::Display )
