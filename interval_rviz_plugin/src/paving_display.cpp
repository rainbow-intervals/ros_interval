/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/properties/int_property.h>
#include <rviz/frame_manager.h>
#include <string>

#include "paving_visual.h"
#include "paving_display.h"
#include "ros/ros.h"

namespace interval_rviz_plugin
{

// The constructor must have no arguments, so we can't give the
// constructor the parameters it needs to fully initialize.
PavingDisplay::PavingDisplay()
{

  domain_color_property_ = new rviz::ColorProperty( "DomainColor", QColor( 0, 0, 0 ),
                                              "Color to draw the domain box.",
                                              this, SLOT( updateColorAndAlpha() ));

  domain_visibility_property_ = new rviz::BoolProperty( "DomainVisible", true,
                                              "Option for the display of the domain box",
                                                this, SLOT( updateVisibility() ));


  // initialisation of the flags and the length.
  flag_received_msg = false;
  flag_init_properties = true;
  length_label = 0;

}

// After the top-level rviz::Display::initialize() does its own setup,
// it calls the subclass's onInitialize() function.  This is where we
// instantiate all the workings of the class.  We make sure to also
// call our immediate super-class's onInitialize() function, since it
// does important stuff setting up the message filter.
void PavingDisplay::onInitialize()
{
  MFDClass::onInitialize();

}

PavingDisplay::~PavingDisplay()
{
}

// Clear the visual by deleting its objects.
void PavingDisplay::reset()
{
  MFDClass::reset();

}

// Set the current color and alpha values for each visual.
void PavingDisplay::updateColorAndAlpha()
{
  Ogre::ColourValue domain_color = domain_color_property_->getOgreColor();

  // Set the color and alpha for each subpaving inside the paving.
  for ( unsigned i=0; i<color_property_list_.size(); i++)
  {
    Ogre::ColourValue color = color_property_list_[i]->getOgreColor();
    float alpha = alpha_property_list_[i]->getFloat();
    bool hide = visibility_property_list_[i]->getBool();

    // Set the color and alpha for each box inside the subpaving.
    if (flag_received_msg && list_sub_display[i])
      visual->setColorAndVisibility( color.r, color.g, color.b, alpha, i, hide);
    list_sub_display[i] = not hide;
  }
  // Set the color of the Domain.
  if (flag_received_msg)
    visual->setDomainColor( domain_color.r, domain_color.g, domain_color.b);
}

// Set the current state of the Domain (Visible or not).
void PavingDisplay::updateVisibility()
{
  bool domain_visible = domain_visibility_property_->getBool();
  if (flag_received_msg)
    visual->setDomainVisibility( domain_visible );
}
// This is our callback to handle an incoming message.
void PavingDisplay::processMessage( const interval_msgs::PavingStamped::ConstPtr& msg )
{
  // Here we call the rviz::FrameManager to get the transform from the
  // fixed frame to the frame in the header of this PavingStamped message.  If
  // it fails, we can't do anything else so we return.
  Ogre::Quaternion orientation;
  Ogre::Vector3 position;

  // Flag of the first message received.
  flag_received_msg = true;

  // Reset properties if incoming message has a different size.
  if (length_label != msg->pav.label.size())
  {
    flag_init_properties = true;
    length_label = msg->pav.label.size();
  }

  // Enter this part if the properties have to be reset.
  if (flag_init_properties)
  {
    ROS_INFO("flag_init_properties");
    for (unsigned i=0; i<color_property_list_.size(); i++)
    {
      ROS_INFO("re_init");
      alpha_property_list_[i].reset();
      visibility_property_list_[i].reset();
      color_property_list_[i].reset();

    }

    list_sub_display.erase(list_sub_display.begin(),list_sub_display.end());
    ROS_INFO("list_display clear");
    color_property_list_.clear();
    ROS_INFO("color_clear");
    alpha_property_list_.clear();
    ROS_INFO("alpha_clear");
    visibility_property_list_.clear();
    for ( unsigned i=0; i<msg->pav.label.size(); i++)
    {
      color_property_list_.push_back(boost::shared_ptr<rviz::ColorProperty>());
      color_property_list_[i].reset(new rviz::ColorProperty( msg->pav.label[i].c_str(),
                                                QColor( i*250/(msg->pav.label.size()-1),
                                                0,
                                                (msg->pav.label.size()-1-i)*250/(msg->pav.label.size()-1) ),
                                                "Color to draw the boxes of the Paving.",
                                                this, SLOT( updateColorAndAlpha() )));

      alpha_property_list_.push_back(boost::shared_ptr<rviz::FloatProperty>());
      alpha_property_list_[i].reset(new rviz::FloatProperty( "Alpha", 0.3,
                                                "0 is fully transparent, 1.0 is fully opaque.",
                                                color_property_list_[i].get()));

      visibility_property_list_.push_back(boost::shared_ptr<rviz::BoolProperty>());
      visibility_property_list_[i].reset(new rviz::BoolProperty( "Hide", false,
                                                  "Option for the display of the subpaving",
                                                  color_property_list_[i].get()));
      list_sub_display.push_back(true);
    }
    flag_init_properties = false;
    ROS_INFO("init_ok");
  }

  if( !context_->getFrameManager()->getTransform( msg->header.frame_id,
                                                  msg->header.stamp,
                                                  position, orientation ))
  {
    ROS_DEBUG( "Error transforming from frame '%s' to frame '%s'",
               msg->header.frame_id.c_str(), qPrintable( fixed_frame_ ));
    return;
  }


  visual.reset(new PavingVisual( context_->getSceneManager(), scene_node_, msg, list_sub_display ));
  // Now set or update the contents of the chosen visual.
  visual->setMessage( msg );
  visual->setFramePosition( position );
  visual->setFrameOrientation( orientation );


  bool domain_visible = domain_visibility_property_->getBool();
  Ogre::ColourValue domain_color = domain_color_property_->getOgreColor();

  for ( unsigned i=0; i<color_property_list_.size(); i++)
  {
    Ogre::ColourValue color = color_property_list_[i]->getOgreColor();
    float alpha = alpha_property_list_[i]->getFloat();
    bool hide = visibility_property_list_[i]->getBool();

    if (list_sub_display[i])
      visual->setColorAndVisibility( color.r, color.g, color.b, alpha, i, hide);

    list_sub_display[i] = not hide;
  }
  visual->setDomainColor( domain_color.r, domain_color.g, domain_color.b);
  visual->setDomainVisibility( domain_visible );


}

} // end namespace interval_rviz_plugin

// Tell pluginlib about this class.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(interval_rviz_plugin::PavingDisplay,rviz::Display )
