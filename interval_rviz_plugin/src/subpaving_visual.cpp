/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/shape.h>
#include <rviz/ogre_helpers/line.h>
#include <cmath>

#include "subpaving_visual.h"

namespace interval_rviz_plugin
{

SubPavingVisual::SubPavingVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node, const interval_msgs::SubPavingStamped::ConstPtr& msg )
{
  scene_manager_ = scene_manager;

  // Ogre::SceneNode s form a tree, with each node storing the
  // transform (position and orientation) of itself relative to its
  // parent.  Ogre does the math of combining those transforms when it
  // is time to render.
  //
  // Here we create a node to store the pose of the SubPavingStamped's header frame
  // relative to the RViz fixed frame.
  frame_node_ = parent_node->createChildSceneNode();

  // We create the arrow object within the frame node so that we can
  // set its position and direction relative to its header frame.
  box_.clear();
  int length = msg->sub.subpaving.size();

  for (unsigned i=0; i<length; i++)
  {
    boost::shared_ptr<rviz::Shape> box;

    box_.push_back(box);

    box_[i].reset(new rviz::Shape( (rviz::Shape::Type)1, scene_manager_, frame_node_ ));
  }

  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i].reset(new rviz::Line( scene_manager_, frame_node_ ));
  }

}

SubPavingVisual::~SubPavingVisual()
{
  // Destroy the frame node since we don't need it anymore.
  scene_manager_->destroySceneNode( frame_node_ );
}

void SubPavingVisual::setMessage( const interval_msgs::SubPavingStamped::ConstPtr& msg )
{
  int length = msg->sub.subpaving.size();

  for (unsigned i=0; i<length; i++)
  {

    float xub = msg->sub.subpaving[i].box[0].ub;
    float xlb = msg->sub.subpaving[i].box[0].lb;
    float yub = msg->sub.subpaving[i].box[1].ub;
    float ylb = msg->sub.subpaving[i].box[1].lb;
    float zub = msg->sub.subpaving[i].box[2].ub;
    float zlb = msg->sub.subpaving[i].box[2].lb;

    // Create an Ogre::Vector3 corresponding to the scale of each box.
    Ogre::Vector3 span( xub-xlb, yub-ylb, zub-zlb );

    // Create an Ogre::Vector3 corresponding to the position of the middle of the box.
    Ogre::Vector3 pos( (xub+xlb)/2.0, (yub+ylb)/2.0, (zub+zlb)/2.0 );

    // Scale the box's span in each dimension along with its length.
    box_[i]->setScale( span );

    // Set the orientation of the cube to match the direction of the
    // center of the box.
    box_[i]->setPosition( pos );

  }

  // Here we create the vectors needed so as to set the domain's lines.
  float xub = msg->sub.domain.box[0].ub;
  float xlb = msg->sub.domain.box[0].lb;
  float yub = msg->sub.domain.box[1].ub;
  float ylb = msg->sub.domain.box[1].lb;
  float zub = msg->sub.domain.box[2].ub;
  float zlb = msg->sub.domain.box[2].lb;

  Ogre::Vector3 lll( xlb, ylb, zlb );
  Ogre::Vector3 llu( xlb, ylb, zub );
  Ogre::Vector3 luu( xlb, yub, zub );
  Ogre::Vector3 lul( xlb, yub, zlb );
  Ogre::Vector3 uul( xub, yub, zlb );
  Ogre::Vector3 uuu( xub, yub, zub );
  Ogre::Vector3 ulu( xub, ylb, zub );
  Ogre::Vector3 ull( xub, ylb, zlb );

  domain_box_[0]->setPoints( lll, llu);
  domain_box_[1]->setPoints( lll, ull);
  domain_box_[2]->setPoints( lll, lul);
  domain_box_[3]->setPoints( ulu, ull);
  domain_box_[4]->setPoints( ulu, uuu);
  domain_box_[5]->setPoints( ulu, llu);
  domain_box_[6]->setPoints( luu, llu);
  domain_box_[7]->setPoints( luu, lul);
  domain_box_[8]->setPoints( luu, uuu);
  domain_box_[9]->setPoints( uul, lul);
  domain_box_[10]->setPoints( uul, uuu);
  domain_box_[11]->setPoints( uul, ull);



}

// Position and orientation are passed through to the SceneNode.
void SubPavingVisual::setFramePosition( const Ogre::Vector3& position )
{
  frame_node_->setPosition( position );
}

void SubPavingVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
{
  frame_node_->setOrientation( orientation );
}

// Color is passed through to each box object.
void SubPavingVisual::setColor( float r, float g, float b, float a)
{
  for (unsigned i=0;i<box_.size();i++)
  {
    box_[i]->setColor( r, g, b, a );
  }
}

// Color is passed through to each line object (Domain).
void SubPavingVisual::setDomainColor( float dr, float dg, float db)
{
  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i]->setColor( dr, dg, db, 1.0 );
  }
}

// Visibility is set for each line of the domain.
void SubPavingVisual::setDomainVisibility( bool visible )
{
  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i]->setVisible( visible );
  }
}

} // end namespace interval_rviz_plugin
