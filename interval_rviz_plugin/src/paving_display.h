/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PAVING_DISPLAY_H
#define PAVING_DISPLAY_H

#ifndef Q_MOC_RUN

#include <rviz/message_filter_display.h>
#include <interval_msgs/Interval.h>
#include <interval_msgs/IntervalVector.h>
#include <interval_msgs/Paving.h>
#include <interval_msgs/PavingStamped.h>
#include <vector>
#endif

namespace Ogre
{
class SceneNode;
}

namespace rviz
{
class ColorProperty;
class FloatProperty;
class IntProperty;
}

// All the source in this plugin is in its own namespace.  This is not
// required but is good practice.
namespace interval_rviz_plugin
{

class PavingVisual;


// Here we declare our new subclass of rviz::Display.  Every display
// which can be listed in the "Displays" panel is a subclass of
// rviz::Display.
//
// PavingDisplay will show several sets of 3D boxes of a paving.
// The origin of the subpaving will be at
// the frame listed in the header of the PavingStamped message,
// and the orientation of the paving corresponding to the orientation of that frame.
//
// The PavingDisplay class itself just implements
// editable parameters, and Display subclass machinery.  The visuals
// themselves are represented by a separate class, PavingVisual. The
// idiom for the visuals is that when the objects exist and they are set hidden, they appear
// in the scene, and when they are deleted, they disappear.
class PavingDisplay: public rviz::MessageFilterDisplay<interval_msgs::PavingStamped>
{
Q_OBJECT
public:
  // Constructor.
  PavingDisplay();
  virtual ~PavingDisplay();

  // Overrides of protected virtual functions from Display.  As much
  // as possible, when Displays are not enabled, they should not be
  // subscribed to incoming data and should not show anything in the
  // 3D view.  These functions are where these connections are made
  // and broken.
protected:
  virtual void onInitialize();

  // A helper to clear this display back to the initial state.
  virtual void reset();

  // These Qt slots get connected to signals indicating changes in the user-editable properties.
private Q_SLOTS:
  void updateColorAndAlpha();
  void updateVisibility();

  // Function to handle an incoming ROS message.
private:
  void processMessage( const interval_msgs::PavingStamped::ConstPtr& msg );

  // Storage for the list of visuals.
  boost::shared_ptr<PavingVisual> visual;

  // User-editable property variables.
  std::vector < boost::shared_ptr<rviz::ColorProperty> > color_property_list_;
  std::vector < boost::shared_ptr<rviz::FloatProperty> > alpha_property_list_;
  std::vector < boost::shared_ptr<rviz::BoolProperty> > visibility_property_list_;

  rviz::ColorProperty* domain_color_property_;
  rviz::BoolProperty* domain_visibility_property_;

  // Flags for initialisation
  bool flag_init_properties;
  bool flag_received_msg;

  // Memory of the length of the last incoming message (0 by default)
  int length_label;

  // List of boolean showing the state of each subpaving (hidden or not)
  std::vector<bool> list_sub_display;
};

} // end namespace interval_rviz_plugin

#endif // VECTOR3_INTERVAL_DISPLAY_H
// %EndTag(FULL_SOURCE)%
