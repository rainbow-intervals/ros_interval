/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SUBPAVING_VISUAL_H
#define SUBPAVING_VISUAL_H

#include <interval_msgs/Interval.h>
#include <interval_msgs/IntervalVector.h>
#include <interval_msgs/SubPaving.h>
#include <interval_msgs/SubPavingStamped.h>
#include <vector>


namespace Ogre
{
class Vector3;
class Quaternion;
}

namespace rviz
{
class Shape;
class Line;

}

namespace interval_rviz_plugin
{

// Declare the visual class for this display.
//
// Each instance of SubPavingVisual represents the visualization of a single
// interval_msgs::SubPavingStamped message.
class SubPavingVisual
{
public:
  // Constructor.  Creates the visual stuff and puts it into the
  // scene, but in an unconfigured state.
  SubPavingVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node, const interval_msgs::SubPavingStamped::ConstPtr& msg );

  // Destructor.  Removes the visual stuff from the scene.
  virtual ~SubPavingVisual();

  // Configure the visual to show the data in the message.
  void setMessage( const interval_msgs::SubPavingStamped::ConstPtr& msg );

  // Set the pose of the coordinate frame the message refers to.
  // These could be done inside setMessage(), but that would require
  // calls to FrameManager and error handling inside setMessage(),
  // which doesn't seem as clean.  This way SubPavingVisual is only
  // responsible for visualization.
  void setFramePosition( const Ogre::Vector3& position );
  void setFrameOrientation( const Ogre::Quaternion& orientation );

  // Set the color and alpha of the visual, which are user-editable
  // parameters and therefore don't come from the SubPaving message.
  void setColor( float r, float g, float b, float a);
  void setDomainColor( float dr, float dg, float db);
  void setDomainVisibility( bool visible );

private:
  // The object implementing the actual set of boxes and the domain's lines objects.
  std::vector< boost::shared_ptr<rviz::Shape> > box_;
  std::vector< boost::shared_ptr<rviz::Line> > domain_box_ = std::vector< boost::shared_ptr<rviz::Line> >(12);;

  // A SceneNode whose pose is set to match the coordinate frame of
  // the SubPaving message header.
  Ogre::SceneNode* frame_node_;

  // The SceneManager, kept here only so the destructor can ask it to
  // destroy the ``frame_node_``.
  Ogre::SceneManager* scene_manager_;
};

} // end namespace interval_rviz_plugin

#endif // POINT_INTERVAL_VISUAL_H
