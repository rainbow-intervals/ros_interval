/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/shape.h>
#include <rviz/ogre_helpers/line.h>
#include <cmath>

#include "paving_visual.h"
#include "ros/ros.h"

namespace interval_rviz_plugin
{

PavingVisual::PavingVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node, const interval_msgs::PavingStamped::ConstPtr& msg, std::vector<bool> list_display )
{
  scene_manager_ = scene_manager;

  // Ogre::SceneNode s form a tree, with each node storing the
  // transform (position and orientation) of itself relative to its
  // parent.  Ogre does the math of combining those transforms when it
  // is time to render.
  //
  // Here we create a node to store the pose of the PavingStamped's header frame
  // relative to the RViz fixed frame.
  frame_node_ = parent_node->createChildSceneNode();

  list_sub_visual = list_display;

  // We create the box and line objects within the frame node so that we can
  // set its position and direction relative to its header frame.
  // Boxes of a subpaving are created if the corresponding list_display boolean
  // is set on true.
  box_.clear();
  int length_paving = msg->pav.paving.size();

  for (unsigned i=0; i<length_paving; i++)
  {
    int length_subpaving = msg->pav.paving[i].subpaving.size();
    box_.push_back( std::vector< boost::shared_ptr<rviz::Shape> >());

    if (list_display[i])
    {
      for (unsigned j=0; j<length_subpaving; j++)
      {
        boost::shared_ptr<rviz::Shape> box;
        box_[i].push_back(box);
        box_[i][j].reset(new rviz::Shape( (rviz::Shape::Type)1, scene_manager_, frame_node_ ));
      }
    }
  }


  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i].reset(new rviz::Line( scene_manager_, frame_node_ ));
  }


}

PavingVisual::~PavingVisual()
{
  // Destroy the frame node since we don't need it anymore.
  scene_manager_->destroySceneNode( frame_node_ );
}

void PavingVisual::setMessage( const interval_msgs::PavingStamped::ConstPtr& msg )
{
  int length_paving = msg->pav.paving.size();

  for (unsigned i=0; i<length_paving; i++)
  {

    int length_subpaving = msg->pav.paving[i].subpaving.size();

    if (list_sub_visual[i])
    {
      for (unsigned j=0; j<length_subpaving; j++)
      {
        float xub = msg->pav.paving[i].subpaving[j].box[0].ub;
        float xlb = msg->pav.paving[i].subpaving[j].box[0].lb;
        float yub = msg->pav.paving[i].subpaving[j].box[1].ub;
        float ylb = msg->pav.paving[i].subpaving[j].box[1].lb;
        float zub = msg->pav.paving[i].subpaving[j].box[2].ub;
        float zlb = msg->pav.paving[i].subpaving[j].box[2].lb;

        // Create an Ogre::Vector3 corresponding to the scale of each box.
        Ogre::Vector3 span( xub-xlb, yub-ylb, zub-zlb );

        // Create an Ogre::Vector3 corresponding to the position of the middle of the box.
        Ogre::Vector3 pos( (xub+xlb)/2.0, (yub+ylb)/2.0, (zub+zlb)/2.0 );

        // Scale the box's span in each dimension along with its length.
        box_[i][j]->setScale( span );

        // Set the orientation of the cube to match the direction of the
        // center of the box.
        box_[i][j]->setPosition( pos );
      }
    }
  }

  // Here we create the vectors needed so as to set the domain's lines.
  float xub = msg->pav.domain.box[0].ub;
  float xlb = msg->pav.domain.box[0].lb;
  float yub = msg->pav.domain.box[1].ub;
  float ylb = msg->pav.domain.box[1].lb;
  float zub = msg->pav.domain.box[2].ub;
  float zlb = msg->pav.domain.box[2].lb;

  Ogre::Vector3 lll( xlb, ylb, zlb );
  Ogre::Vector3 llu( xlb, ylb, zub );
  Ogre::Vector3 luu( xlb, yub, zub );
  Ogre::Vector3 lul( xlb, yub, zlb );
  Ogre::Vector3 uul( xub, yub, zlb );
  Ogre::Vector3 uuu( xub, yub, zub );
  Ogre::Vector3 ulu( xub, ylb, zub );
  Ogre::Vector3 ull( xub, ylb, zlb );

  domain_box_[0]->setPoints( lll, llu);
  domain_box_[1]->setPoints( lll, ull);
  domain_box_[2]->setPoints( lll, lul);
  domain_box_[3]->setPoints( ulu, ull);
  domain_box_[4]->setPoints( ulu, uuu);
  domain_box_[5]->setPoints( ulu, llu);
  domain_box_[6]->setPoints( luu, llu);
  domain_box_[7]->setPoints( luu, lul);
  domain_box_[8]->setPoints( luu, uuu);
  domain_box_[9]->setPoints( uul, lul);
  domain_box_[10]->setPoints( uul, uuu);
  domain_box_[11]->setPoints( uul, ull);


}

// Position and orientation are passed through to the SceneNode.
void PavingVisual::setFramePosition( const Ogre::Vector3& position )
{
  frame_node_->setPosition( position );
}

void PavingVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
{
  frame_node_->setOrientation( orientation );
}

// Color is passed through to each box.
void PavingVisual::setColorAndVisibility( float r, float g, float b, float a, unsigned subpaving, bool hide)
{
  if (!hide)
  {
    for (unsigned j=0;j<box_[subpaving].size();j++)
      box_[subpaving][j]->setColor( r, g, b, a );
  }
  else
  {
    for (unsigned j=0;j<box_[subpaving].size();j++)
      box_[subpaving][j]->setColor( r, g, b, 0.0 );
  }

}

void PavingVisual::setDomainColor( float dr, float dg, float db)
{
  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i]->setColor( dr, dg, db, 1.0 );
  }

}

void PavingVisual::setDomainVisibility( bool visible )
{
  for (unsigned i=0;i<domain_box_.size();i++)
  {
    domain_box_[i]->setVisible( visible );
  }

}

} // end namespace interval_rviz_plugin
