/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreVector3.h>
#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <rviz/ogre_helpers/arrow.h>
#include <rviz/ogre_helpers/shape.h>
#include <cmath>

#include "vector3_interval_visual.h"

namespace interval_rviz_plugin
{

Vector3IntervalVisual::Vector3IntervalVisual( Ogre::SceneManager* scene_manager, Ogre::SceneNode* parent_node )
{
  scene_manager_ = scene_manager;

  // Ogre::SceneNode s form a tree, with each node storing the
  // transform (position and orientation) of itself relative to its
  // parent.  Ogre does the math of combining those transforms when it
  // is time to render.
  //
  // Here we create a node to store the pose of the Vector3IntervalStamped's header frame
  // relative to the RViz fixed frame.
  frame_node_ = parent_node->createChildSceneNode();

  // We create the arrow objects and the box within the frame node so that we can
  // set their position and direction relative to their header frame.
  box_.reset(new rviz::Shape((rviz::Shape::Type)1, scene_manager_, frame_node_ ));
  for (unsigned i=0;i<arrow_.size();i++)
  {
    arrow_[i].reset(new rviz::Arrow( scene_manager_, frame_node_ ));
  }

}

Vector3IntervalVisual::~Vector3IntervalVisual()
{
  // Destroy the frame node since we don't need it anymore.
  scene_manager_->destroySceneNode( frame_node_ );
}

void Vector3IntervalVisual::setMessage( const interval_msgs::Vector3IntervalStamped::ConstPtr& msg )
{
  float xub = msg->vector.x.ub;
  float xlb = msg->vector.x.lb;
  float yub = msg->vector.y.ub;
  float ylb = msg->vector.y.lb;
  float zub = msg->vector.z.ub;
  float zlb = msg->vector.z.lb;



  // Create an Ogre::Vector3.
  Ogre::Vector3 span( xub-xlb, yub-ylb, zub-zlb );

  // Create an Ogre::Vector3.
  Ogre::Vector3 pos( (xub+xlb)/2.0, (yub+ylb)/2.0, (zub+zlb)/2.0 );

  // Scale the box's span in each dimension along with its length.
  box_->setScale( span );

  // Set the orientation of the cube to match the position of the
  // center of the box.
  box_->setPosition( pos );

  // Set all the vectors representing the corners of the box
  Ogre::Vector3 lll( xlb, ylb, zlb );
  Ogre::Vector3 llu( xlb, ylb, zub );
  Ogre::Vector3 luu( xlb, yub, zub );
  Ogre::Vector3 lul( xlb, yub, zlb );
  Ogre::Vector3 uul( xub, yub, zlb );
  Ogre::Vector3 uuu( xub, yub, zub );
  Ogre::Vector3 ulu( xub, ylb, zub );
  Ogre::Vector3 ull( xub, ylb, zlb );

  // Set the directions of all the arrows
  arrow_[0]->setDirection( lll );
  arrow_[1]->setDirection( llu );
  arrow_[2]->setDirection( luu );
  arrow_[3]->setDirection( lul );
  arrow_[4]->setDirection( uul );
  arrow_[5]->setDirection( uuu );
  arrow_[6]->setDirection( ulu );
  arrow_[7]->setDirection( ull );

  arrow_[8]->setDirection( pos );

  // Set the lengths of the previous vectors
  float len_lll = lll.length();
  float len_llu = llu.length();
  float len_luu = luu.length();
  float len_lul = lul.length();
  float len_uul = uul.length();
  float len_uuu = uuu.length();
  float len_ulu = ulu.length();
  float len_ull = ull.length();
  float len_pos = pos.length();

  // Represent the minimum span of the 3 intervals
  float min_span = fmin(xub-xlb, fmin(yub-ylb, zub-zlb));

  // Set the length and the width of each arrow
  arrow_[0]->set( 0.8*len_lll, 0.2*min_span, 0.2*len_lll, 0.4*min_span);
  arrow_[1]->set( 0.8*len_llu, 0.2*min_span, 0.2*len_llu, 0.4*min_span);
  arrow_[2]->set( 0.8*len_luu, 0.2*min_span, 0.2*len_luu, 0.4*min_span);
  arrow_[3]->set( 0.8*len_lul, 0.2*min_span, 0.2*len_lul, 0.4*min_span);
  arrow_[4]->set( 0.8*len_uul, 0.2*min_span, 0.2*len_uul, 0.4*min_span);
  arrow_[5]->set( 0.8*len_uuu, 0.2*min_span, 0.2*len_uuu, 0.4*min_span);
  arrow_[6]->set( 0.8*len_ulu, 0.2*min_span, 0.2*len_ulu, 0.4*min_span);
  arrow_[7]->set( 0.8*len_ull, 0.2*min_span, 0.2*len_ull, 0.4*min_span);

  arrow_[8]->set( 0.8*len_pos, 0.2*min_span, 0.2*len_pos, 0.4*min_span);

}

// Position and orientation are passed through to the SceneNode.
void Vector3IntervalVisual::setFramePosition( const Ogre::Vector3& position )
{
  frame_node_->setPosition( position );
}

void Vector3IntervalVisual::setFrameOrientation( const Ogre::Quaternion& orientation )
{
  frame_node_->setOrientation( orientation );
}

// Color is passed through to the Arrow and Shape( Cube ) objects .
void Vector3IntervalVisual::setBoxColor( float r, float g, float b, float a)
{
  box_->setColor( r, g, b, a );
}

void Vector3IntervalVisual::setBoundariesVectorsColor( float r, float g, float b, float a, bool hide)
{
  if (hide)
  {
    for (unsigned i=0; i<arrow_.size()-1; i++)
    {
      arrow_[i]->setColor( r, g, b, 0.0);
    }
  }
  else
  {
    for (unsigned i=0; i<arrow_.size()-1; i++)
    {
      arrow_[i]->setColor( r, g, b, a);
    }
  }
}

void Vector3IntervalVisual::setCentralVectorColor( float r, float g, float b, float a, bool hide)
{
  if (hide)
    arrow_[8]->setColor( r, g, b, 0.0);
  else
    arrow_[8]->setColor( r, g, b, a);
}

} // end namespace interval_rviz_plugin
