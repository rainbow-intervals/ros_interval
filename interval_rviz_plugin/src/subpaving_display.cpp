/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <OGRE/OgreSceneNode.h>
#include <OGRE/OgreSceneManager.h>

#include <tf/transform_listener.h>

#include <rviz/visualization_manager.h>
#include <rviz/properties/color_property.h>
#include <rviz/properties/float_property.h>
#include <rviz/properties/int_property.h>
#include <rviz/frame_manager.h>

#include "subpaving_visual.h"

#include "subpaving_display.h"

namespace interval_rviz_plugin
{

// BEGIN_TUTORIAL
// The constructor must have no arguments, so we can't give the
// constructor the parameters it needs to fully initialize.
SubPavingDisplay::SubPavingDisplay()
{
  color_property_ = new rviz::ColorProperty( "Color", QColor( 250, 0, 0 ),
                                             "Color to draw the boxes of the subpaving.",
                                             this, SLOT( updateColorAndAlpha() ));
  domain_color_property_ = new rviz::ColorProperty( "DomainColor", QColor( 0, 0, 0 ),
                                              "Color to draw the domain box.",
                                              this, SLOT( updateColorAndAlpha() ));

  alpha_property_ = new rviz::FloatProperty( "Alpha", 0.3,
                                             "0 is fully transparent, 1.0 is fully opaque.",
                                             this, SLOT( updateColorAndAlpha() ));

  domain_visibility_property_ = new rviz::BoolProperty( "DomainVisible", true,
                                             "Option for the display of the domain box",
                                              this, SLOT( updateVisibility() ));


//Set of min and max for both alpha parameters
  alpha_property_->setMin( 0 );
  alpha_property_->setMax( 1 );

// Init the Flag for incoming message.
flag_received_msg = false;
}

// After the top-level rviz::Display::initialize() does its own setup,
// it calls the subclass's onInitialize() function.  This is where we
// instantiate all the workings of the class.  We make sure to also
// call our immediate super-class's onInitialize() function, since it
// does important stuff setting up the message filter.

void SubPavingDisplay::onInitialize()
{
  MFDClass::onInitialize();
}

SubPavingDisplay::~SubPavingDisplay()
{
}

// Clear the visual by deleting its objects.
void SubPavingDisplay::reset()
{
  MFDClass::reset();

}

// Set the current color and alpha values for each visual.
void SubPavingDisplay::updateColorAndAlpha()
{
  float alpha = alpha_property_->getFloat();
  Ogre::ColourValue color = color_property_->getOgreColor();
  Ogre::ColourValue domain_color = domain_color_property_->getOgreColor();

 if (flag_received_msg)
 {
   visual->setColor( color.r, color.g, color.b, alpha );
   visual->setDomainColor( domain_color.r, domain_color.g, domain_color.b);
 }
}

void SubPavingDisplay::updateVisibility()
{
  if (flag_received_msg)
  {
    bool domain_visible = domain_visibility_property_->getBool();
    visual->setDomainVisibility( domain_visible );
  }
}



// This is our callback to handle an incoming message.
void SubPavingDisplay::processMessage( const interval_msgs::SubPavingStamped::ConstPtr& msg )
{
  // Here we call the rviz::FrameManager to get the transform from the
  // fixed frame to the frame in the header of this SubPavingStamped message.  If
  // it fails, we can't do anything else so we return.
  Ogre::Quaternion orientation;
  Ogre::Vector3 position;

  flag_received_msg = true;

  if( !context_->getFrameManager()->getTransform( msg->header.frame_id,
                                                  msg->header.stamp,
                                                  position, orientation ))
  {
    ROS_DEBUG( "Error transforming from frame '%s' to frame '%s'",
               msg->header.frame_id.c_str(), qPrintable( fixed_frame_ ));
    return;
  }


  visual.reset(new SubPavingVisual( context_->getSceneManager(), scene_node_, msg ));
  // Now set or update the contents of the chosen visual.
  visual->setMessage( msg );
  visual->setFramePosition( position );
  visual->setFrameOrientation( orientation );

  float alpha = alpha_property_->getFloat();
  bool domain_visible = domain_visibility_property_->getBool();
  Ogre::ColourValue color = color_property_->getOgreColor();
  Ogre::ColourValue domain_color = domain_color_property_->getOgreColor();

  visual->setColor( color.r, color.g, color.b, alpha);
  visual->setDomainColor( domain_color.r, domain_color.g, domain_color.b);
  visual->setDomainVisibility( domain_visible );


}

} // end namespace interval_rviz_plugin

// Tell pluginlib about this class.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(interval_rviz_plugin::SubPavingDisplay,rviz::Display )
