interval
========

This repository provides packages and tools related to the use of *intervals* in 
ROS (the Robot Operating System).

* interval_msgs
* interval_rviz_plugins
* interval_tools
